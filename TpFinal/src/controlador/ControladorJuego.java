package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import modelo.AtacarHilo;
import modelo.Comienzo;
import modelo.EspecialAlquimistaHilo;
import modelo.GameOver;
import modelo.OrgBatalla;
import modelo.PerAlquimista;
import modelo.PerBerserker;
import modelo.PerSlave;
import modelo.Personaje;
import modelo.RondaN;
import modelo.Usuario;
import vista.Juego;
import vista.Menu;

public class ControladorJuego implements ActionListener
{

	private Juego juego;
	private Personaje p; // personaje del usuario, se setea desde la vista anterior
	private Personaje pe;// persoonaje enemigo
	//private String usuario; //id del usuario que se paso desde la vista anterior
	private Boolean flag = true; //batalla continua
	private Integer num;
	/*
	 * se usa para sacar el valor de ataque y setear la barra de vida
	 * como hace un random al atacar, si se lo vuelve a llamar no estariamos teniendo el verdadero valor 
	 * que se obtuvo al principiopara poder setear la vida y a la vez sacarle vida al contrincante (o viceversa)
	 */
	
	private Integer defensa = 0;
	/*
	 * 0 = nadie bloquea
	 * 1 = bloquea usuario
	 * 2 = bloquea oponente
	 */
	private Usuario u; //debo pasarle el score y la id de usuario
	private Integer ronda = 0;
	
	private ArrayList<Personaje> arreglo = new ArrayList<Personaje>(); //es un hilo 
	
	
	
	////////////////////////////////////////////////////
	///////////// CONSTRUCTOR //////////////////////////
	////////////////////////////////////////////////////
	
	public ControladorJuego(Personaje i) 
	{
		this.setJuego(new Juego(this));
		this.getJuego().setVisible(true);
		
		OrganizarBatallaArreglo(); //se organiza la batalla, el arreglo de personajes
		
		personajeEnemigo(); //se setea el personaje enemigo
		this.setP(i); //setea personaje del jugador con el que eligio anteriormente
		
		setearIcono();
		setearBarraVida();
		
		Comienzo c = new Comienzo(this);
		c.start();
		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		JButton btn = (JButton) e.getSource();
		
		if (btn.getText().equals("ataque"))
		{
			System.out.println("USUARIO ATACA");
			
			this.setNum( this.getP().ataque().intValue()); //se obtiene valor de ataque
			AtacarHilo attack = new AtacarHilo(this, this.getP(),1);
			
			//AtacarHilo attack = new AtacarHilo(this);
			attack.start();
			
			if (this.getDefensa() == 2) //enemigo bloquea
			{
				this.getPe().defensa(this.getNum().doubleValue());
				this.getJuego().getBarraDefensaEnemigo().setValue(this.getPe().getDefensa().intValue());
				this.getJuego().getBarraDefensaEnemigo().setString(String.valueOf(this.getPe().getDefensa().intValue()));
				this.setDefensa(0);
				
			}
			else //enemigo no bloqueo
			{
				if (!this.getPe().recibirDaņo(this.getNum().doubleValue())) //si retorna falso es que murio
				{
					this.setFlag(false);
					
				}
				
				this.getJuego().getBarraVidaEnemigo().setString(String.valueOf(this.getPe().getVida().intValue())); //se setea el texto de la barra de vida
				System.out.println("vida enemigo " + this.getPe().getVida()+ "\n"); //ver vida del enemigo

				this.getJuego().getBarraVidaEnemigo().setValue(this.getPe().getVida().intValue());//this.getJuego().getBarraVidaEnemigo().getValue() - this.getNum());

				//desactivarBotones();
			}
			this.getJuego().getBarraVidaJugador().setString(String.valueOf(this.getP().getVida().intValue())); //si es alquimista se setea la vida cuando se cura
			this.getJuego().getBarraVidaJugador().setValue(this.getP().getVida().intValue());
			
		}
		else if (btn.getText().equals("bloqueo"))
		{
			System.out.println("USUARIO DEFIENDE");
			this.setDefensa(1); //
		}
		else if (btn.getText().equals("especial"))
		{
			System.out.println("USUARIO ESPECIAL");
			this.setNum( this.getP().ataqueEspecial().intValue()); //se obtiene valor del ataque especial
			
			EspecialAlquimistaHilo esp = new EspecialAlquimistaHilo(this,this.getP(),1);
			esp.start();
			
			if (this.getDefensa() == 2) //enemigo bloquea
			{
				this.getPe().defensa(this.getNum().doubleValue());
				this.getJuego().getBarraDefensaEnemigo().setValue(this.getPe().getDefensa().intValue());
				this.getJuego().getBarraDefensaEnemigo().setString(String.valueOf(this.getPe().getDefensa().intValue()));
				this.setDefensa(0);
			}
			else //enemigo no bloque
			{
				if (!this.getPe().recibirDaņo(this.getNum().doubleValue())) //si retorna falso es que murio
				{
					this.setFlag(false);
				}
				
				this.getJuego().getBarraVidaEnemigo().setString(String.valueOf(this.getPe().getVida().intValue())); //se setea el texto de la barra de vida
				System.out.println("vida enemigo " + this.getPe().getVida()+ "\n"); //ver vida del enemigo

				this.getJuego().getBarraVidaEnemigo().setValue(this.getPe().getVida().intValue());
			}
		}
		
		Batalla();
		
	}

	
	/////////////////////////////////////////////////////////////////////////
	///////////////////// BATALLA CONTROLAR /////////////////////////////////
	/////////////////////////////////////////////////////////////////////////
	
	public void OrganizarBatallaArreglo()
	{
		this.setArreglo(jefes(asignarPersonajes()));
		
	}
	
	
	public void Batalla()
	{
		if (!this.getFlag())
		{
			desactivarBotones();
			System.out.println("\n" +"Se desactivaron los botones");
			/*
			 * ENEMIGO MURIO
			 * SE DEBE PASAR AL SIGUIENTE
			 * Y PONER EL TRUE EL FLAG
			 */
			this.setRonda(this.getRonda() + 1);
			if (this.getRonda() <= 14)
			{
				personajeEnemigo();
				//RONDA
				RondaN round = new RondaN(this);
				round.start();
				
				
				this.setFlag(true);
				//activarBotones(); BORRAR SI TRAE PROBLEMAS
			}
			else
			{
				this.setFlag(false);
			}
			
			
		}
		else
		{
			
		Random rnd = new Random();
		
		switch (rnd.nextInt(3)) {
		case 0:
		//ataca
		
		System.out.println("ENEMIGO ATACA");
		this.setNum( this.getPe().ataque().intValue());
		AtacarHilo attack = new AtacarHilo(this, this.getP(),2);
		attack.start();
			
		if (this.getDefensa() == 1) // jugador bloquea
		{
			this.getP().defensa(this.getNum().doubleValue());
			this.getJuego().getBarraDefensaJugador().setValue(this.getP().getDefensa().intValue());
			this.getJuego().getBarraDefensaJugador().setString(String.valueOf(this.getP().getDefensa().intValue()));
			this.setDefensa(0);
		}
		else
		{
			if(!this.getP().recibirDaņo(this.getNum().doubleValue()))
			{
				this.setFlag(false);				
			}
			this.getJuego().getBarraVidaJugador().setString(String.valueOf(this.getP().getVida().intValue()));
			
			System.out.println("vida usuario " + this.getP().getVida() + "\n"); //ver vida del jugador
			this.getJuego().getBarraVidaJugador().setValue(this.getP().getVida().intValue());
		}
		
		this.getJuego().getBarraVidaEnemigo().setString(String.valueOf(this.getPe().getVida().intValue())); //si es alquimista se setea la vida cuando se cura
		this.getJuego().getBarraVidaEnemigo().setValue(this.getPe().getVida().intValue());
		
			break;
				
		case 1:
		//bloquea
			this.setDefensa(2);
			System.out.println("ENEMIGO BLOQUE "+ "\n");
			break;
					
		case 2:
		//especial
			System.out.println("ENEMIGO TIRA ESPECIAL"+ "\n");
			this.setNum( this.getPe().ataqueEspecial().intValue()); //se obtiene valor del ataque especial
			
			EspecialAlquimistaHilo esp = new EspecialAlquimistaHilo(this,this.getPe(),2);
			esp.start();
			
			if (this.getDefensa() == 1) //jugador bloquea
			{
				this.getP().defensa(this.getNum().doubleValue());
				this.getJuego().getBarraDefensaJugador().setValue(this.getP().getDefensa().intValue());
				this.getJuego().getBarraDefensaJugador().setString(String.valueOf(this.getP().getDefensa().intValue()));
				this.setDefensa(0);
			}
			else //enemigo no bloque
			{
				if (!this.getP().recibirDaņo(this.getNum().doubleValue())) //si retorna falso es que murio
				{
					this.setFlag(false);
				}
				
				this.getJuego().getBarraVidaJugador().setString(String.valueOf(this.getP().getVida().intValue())); //se setea el texto de la barra de vida
				System.out.println("vida jugador " + this.getP().getVida()+ "\n"); //ver vida del enemigo

				this.getJuego().getBarraVidaJugador().setValue(this.getP().getVida().intValue());
			}
			
			
			break;
		default:
			break;
		}
		}
		
		if (!this.getFlag())
		{
			desactivarBotones();
			System.out.println("\n" +"Se desactivaron los botones");
			/*
			 * USUARIO MURIO
			 * PASAR EL i DEL ARREGLO PARA SABER SI SE GUARDA O NO
			 */
			GameOver go = new GameOver(this);
			go.start();
			
			//FALTA PONER IMAGEn
		}
		
	}
	
	/////////////////////////////////////////////////////////////////////////
	///////////////////// ASIGNACION DE PERSONAJES //////////////////////////
	/////////////////////////////////////////////////////////////////////////
	
	void personajeEnemigo() 
	{
		this.setPe(this.getArreglo().get(this.getRonda()));
	}
	
	private ArrayList<Personaje> asignarPersonajes() {
		Random ran = new Random();
		ArrayList<Personaje> pe = new ArrayList<Personaje>();
		for (int i = 0; i <= 14; i++) {
			switch (ran.nextInt(3)) {
			case 0:
				pe.add(new PerSlave(800.0, 450.0, 150.0));
				pe.get(i).setNombrePersonaje("Slave");
				break;
			case 1:
				pe.add(new PerBerserker(900.0, 800.0, 90.0));
				pe.get(i).setNombrePersonaje("Berserker");
				break;
			case 2:
				pe.add(new PerAlquimista(710.0, 400.0, 120.0));
				pe.get(i).setNombrePersonaje("Alquimista");
				break;
			}
			
		}
		return pe;
	}
	
	private ArrayList<Personaje> jefes(ArrayList<Personaje> p) {
		Personaje pers = p.get(4);
		pers.setAtaque((pers.getAtaque() + (pers.getAtaque() * 0.10)));
		pers.setVida((pers.getVida() + (pers.getVida() * 0.10)));
		pers.setDefensa((pers.getDefensa() + (pers.getDefensa() * 0.10)));
		p.set(4, pers); //
		pers = p.get(9);//p.set(9, pers);
		pers.setAtaque((pers.getAtaque() + (pers.getAtaque() * 0.15)));
		pers.setVida((pers.getVida() + (pers.getVida() * 0.15)));
		pers.setDefensa((pers.getDefensa() + (pers.getDefensa() * 0.15)));
		p.set(9, pers);
		pers = p.get(14);//p.set(14, pers);
		pers.setAtaque((pers.getAtaque() + (pers.getAtaque() * 0.20)));
		pers.setVida((pers.getVida() + (pers.getVida() * 0.20)));
		pers.setDefensa((pers.getDefensa() + (pers.getDefensa() * 0.20)));
		p.set(14, pers);
		
		return p;
	}
	
	/////////////////////////////////////////////////////////////////////////
	///////////////////// SETEOS DE COMPONENTES /////////////////////////////
	/////////////////////////////////////////////////////////////////////////	
	
	public void activarBotones ()
	{
		this.getJuego().getBtnAtaque().setEnabled(true);
		this.getJuego().getBtnBloqueo().setEnabled(true);
		this.getJuego().getBtnEspecial().setEnabled(true);
		
	}
	
	public void desactivarBotones()
	{
		this.getJuego().getBtnAtaque().setEnabled(false);
		this.getJuego().getBtnBloqueo().setEnabled(false);
		this.getJuego().getBtnEspecial().setEnabled(false);
	}
	
	public void setearIcono()
	{
		switch (this.getP().getNombrePersonaje()) {
		case "Slave":
			this.getJuego().getLblJugador().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/SlaAtack 1.png")));
			break;

		case "Berserker":
			this.getJuego().getLblJugador().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/BerkAtack 1.png")));
			break;
		case "Alquimista":
			this.getJuego().getLblJugador().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/AlqAtack 1.png")));
			break;
		default:
			break;
		}
		
		switch (this.getPe().getNombrePersonaje()) {
		case "Slave":
			this.getJuego().getLblEnemigo().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/SlaAtack 1.png")));
			break;

		case "Berserker":
			this.getJuego().getLblEnemigo().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/BerkAtack 1.png")));
			break;
		case "Alquimista":
			this.getJuego().getLblEnemigo().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/AlqAtack 1.png")));
			break;
		default:
			break;
		}

	}
	
	
	public void setearBarraVida()
	{
		
		this.getP().setVida(this.getP().getVida() + this.getP().getVida()*0.09);
		this.getP().setAtaque(this.getP().getAtaque() + this.getP().getAtaque()*0.02);
		
		this.getJuego().getBarraVidaJugador().setMaximum(this.getP().getVida().intValue()); //la barra setea su maximo con la vida del jugador
		this.getJuego().getBarraVidaEnemigo().setMaximum(this.getPe().getVida().intValue());
		
		this.getJuego().getBarraVidaJugador().setValue(this.getP().getVida().intValue());
		this.getJuego().getBarraVidaEnemigo().setValue(this.getPe().getVida().intValue());
		/*
		System.out.println(this.getJuego().getBarraVidaJugador().getMaximum() + " Vida Maxima Usuario");
		System.out.println(this.getJuego().getBarraVidaEnemigo().getMaximum() + " Vida Maxima Enemigo");
		System.out.println(this.getJuego().getBarraVidaJugador().getValue() + " valor de la barra");
		System.out.println(this.getJuego().getBarraVidaEnemigo().getValue() + " valor de la barra" + "\n");
		*/
		this.getJuego().getBarraVidaEnemigo().setString(this.getPe().getVida().toString());
		this.getJuego().getBarraVidaJugador().setString(this.getP().getVida().toString());
		
		this.getJuego().getBarraDefensaJugador().setMaximum(this.getP().getDefensa().intValue());
		this.getJuego().getBarraDefensaEnemigo().setMaximum(this.getPe().getDefensa().intValue());
		
		this.getJuego().getBarraDefensaJugador().setValue(this.getP().getDefensa().intValue());
		this.getJuego().getBarraDefensaEnemigo().setValue(this.getPe().getDefensa().intValue());
		
		this.getJuego().getBarraDefensaJugador().setString(this.getP().getDefensa().toString());
		this.getJuego().getBarraDefensaEnemigo().setString(this.getPe().getDefensa().toString());
	}
	
	
	
	public Juego getJuego() {
		return juego;
	}

	public void setJuego(Juego juego) {
		this.juego = juego;
	}

	public Personaje getP() {
		return p;
	}

	public void setP(Personaje p) {
		this.p = p;
	}

	/*
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}*/

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	public Personaje getPe() {
		return pe;
	}

	public void setPe(Personaje pe) {
		this.pe = pe;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public ArrayList<Personaje> getArreglo() {
		return arreglo;
	}

	public void setArreglo(ArrayList<Personaje> arreglo) {
		this.arreglo = arreglo;
	}

	public Integer getDefensa() {
		return defensa;
	}

	public void setDefensa(Integer defensa) {
		this.defensa = defensa;
	}


	public Usuario getU() {
		return u;
	}


	public void setU(Usuario u) {
		this.u = u;
	}


	public Integer getRonda() {
		return ronda;
	}


	public void setRonda(Integer ronda) {
		this.ronda = ronda;
	}


}
