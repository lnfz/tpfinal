package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import modelo.Usuario;
import modelo.UsuarioDaoImpl;
import vista.VistaTabla;

public class ControladorUsuarioDaoImpl implements WindowListener, ActionListener
{
	
	private UsuarioDaoImpl udi;
	private VistaTabla vt;
	
	public ControladorUsuarioDaoImpl()
	{
		this.setUdi(new UsuarioDaoImpl());
		this.setVt(new VistaTabla(this));
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		if(arg0.getSource() == this.getVt().getBtnVolver()) {
			ContrMenu conmen = new ContrMenu();
			conmen.getMen().setVisible(true);
			//conmen.getMen().getMusica().start();
			vt.dispose();
		}
		
	}
	
	
	@Override
	public void windowActivated(WindowEvent arg0) {
		String [] columnas = {"id_legajo","nombre","score","personaje"}; //las columnas del nuevo modelo
		
		DefaultTableModel modelo=new DefaultTableModel(); //creo un nuevo modelo
		
		modelo.setColumnIdentifiers(columnas); //seteo las columnas del nuevo modelo
		
		List<Usuario> usuarios = this.getUdi().buscarUsuario();
		
		for (Usuario usuario : usuarios) {
			Object[]row = new Object[4];
			row[0]=usuario.getId_usuario();
			row[1]=usuario.getNombre();
			row[2]=usuario.getScore();
			row[3]=usuario.getPersonaje();
			modelo.addRow(row);
			
		}
		this.getVt().getUsuarios().setModel(modelo); //setea el modelo de la tabla, de esa vista con lo cargado anteriormente
		
	}
	
	
	public UsuarioDaoImpl getUdi() {
		return udi;
	}
	public void setUdi(UsuarioDaoImpl udi) {
		this.udi = udi;
	}
	public VistaTabla getVt() {
		return vt;
	}
	public void setVt(VistaTabla vt) {
		this.vt = vt;
	}

	
	

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	

}
