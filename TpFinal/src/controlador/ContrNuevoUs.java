package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import modelo.PerAlquimista;
import modelo.PerBerserker;
import modelo.PerSlave;
import modelo.Usuario;
import modelo.UsuarioDaoImpl;
import vista.NuevoUsuario;

public class ContrNuevoUs implements ActionListener{

	private NuevoUsuario nu;
	private Integer ranura;
	private UsuarioDaoImpl udi;
	private Usuario u;

	public ContrNuevoUs() {
		super();
		this.setNu(new NuevoUsuario(this));
		this.getNu().setVisible(true);
		this.setUdi(new UsuarioDaoImpl());
	}

	public NuevoUsuario getNu() {
		return nu;
	}

	public void setNu(NuevoUsuario nu) {
		this.nu = nu;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		JButton btn = (JButton) e.getSource();
		
		if (btn.getText().equals("Cancel")){
			ControladorSelectRanura csr = new ControladorSelectRanura();
			csr.getSr().setModal(true); //esto del modal averiguar bien
			csr.getSr().setVisible(true);
			nu.dispose();
		} else if (btn.getText().equals("OK")) { 
			
			if (!this.getNu().getTxtNombre().getText().equals("")) //comprueba que se escriba algo
			{
				//this.setU(new Usuario ("4",this.getNu().getTxtNombre().getText(),"",String.valueOf(new Random().nextInt(999))));
				
				/*
				 * crea un usuario y le da los valores correspondientes en los parametros
				 * 1) incrementa el id_usuario en 1 (desde el ultimo)
				 * 2) obtiene el nombre que se escribe en el texfield
				 * 3) deja en blanco el apellido y el score
				 */
				
				//id, nombre,  score, personaje
				this.setU(new Usuario (String.valueOf(this.getUdi().incrementarId()),this.getNu().getTxtNombre().getText(),"0",this.getNu().getComboBox().getSelectedItem().toString()));
				
				this.getU().setVida("0");
				this.getU().setDefensa("0");
				this.getU().setAtaque("0");
				//getpersonaje es string
				//getp es del personaje del usuario
				this.getU().setRonda("0");
				this.getU().setDificultad(this.getNu().getComboBox_1().getSelectedItem().toString());
				
				this.getUdi().agregarUsuario(this.getU());
				
				seleccionPersonaje();
				
				this.getUdi().agregarRanura(this.getU(), this.getRanura());
				
				ControladorJuego cj = new ControladorJuego(this.getU().getP());
				cj.getJuego().setFocusable(true); //esto del modal averiguar bien
				cj.getJuego().setVisible(true);
				cj.setU(this.getU());
				nu.dispose();
				
				//cambiar a usuario y setearle los atributos
			}else{
				JOptionPane.showMessageDialog(null, "Debe ingresar un nombre de Usuario.", "Error", 2);
			}
			
		}
	}

	public void seleccionPersonaje()
	{
		switch (this.getU().getPersonaje()) {
		case "Alquimista":
			System.out.println("Se eligio alquimista");
			this.getU().setP(new PerAlquimista(710.0, 400.0, 120.0));
			this.getU().getP().setNombrePersonaje("Alquimista");
			
			break;
			
		case "Berserker":
			System.out.println("Se eligio berserker");
			this.getU().setP(new PerBerserker(900.0, 800.0, 90.0));
			this.getU().getP().setNombrePersonaje("Berserker");
			break;
			
		case "Slave":
			System.out.println("Se eligio Esclavo");
			this.getU().setP(new PerSlave(800.0, 450.0, 150.0));
			this.getU().getP().setNombrePersonaje("Slave");
		default:
			break;
		}
	}
	
	public Integer getRanura() {
		return ranura;
	}

	public void setRanura(Integer ranura) {
		this.ranura = ranura;
	}

	public UsuarioDaoImpl getUdi() {
		return udi;
	}

	public void setUdi(UsuarioDaoImpl udi) {
		this.udi = udi;
	}

	public Usuario getU() {
		return u;
	}

	public void setU(Usuario u) {
		this.u = u;
	}
	
}
