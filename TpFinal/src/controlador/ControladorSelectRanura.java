package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import modelo.UsuarioDaoImpl;
import vista.SelectRanura;

public class ControladorSelectRanura implements ActionListener {

	private SelectRanura sr;

	public ControladorSelectRanura() {
		this.setSr(new SelectRanura(this));
		this.getSr().setVisible(true);
	}

	public SelectRanura getSr() {
		return sr;
	}

	public void setSr(SelectRanura sr) {
		this.sr = sr;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		if (btn.getText().equals("CANCELAR")) {
			ContrMenu conmen = new ContrMenu();
			conmen.getMen().setFocusable(true);
			conmen.getMen().setVisible(true);
			//conmen.getMen().getMusica().start();
			sr.dispose();
		} else if (btn.getText().equals("SIGUIENTE")) {
			if (this.getSr().getBtn1().isSelected())
			{
				ContrNuevoUs conus = new ContrNuevoUs();
				conus.getNu().setVisible(true);
				conus.getNu().setFocusable(true);
				conus.setRanura(1);
				sr.dispose();
			} 
			else if (this.getSr().getBtn2().isSelected())
			{
				ContrNuevoUs conus = new ContrNuevoUs();
				conus.getNu().setVisible(true);
				conus.getNu().setFocusable(true);
				conus.setRanura(2);
				sr.dispose();	
			}
			else if (this.getSr().getBtn3().isSelected())
			{
				ContrNuevoUs conus = new ContrNuevoUs();
				conus.getNu().setVisible(true);
				conus.getNu().setFocusable(true);
				conus.setRanura(3);
				sr.dispose();
			} 
			else 
			{
				JOptionPane.showMessageDialog(null, "Debe seleccionar una ranura", "Error", 2);
			}
			
		}

	}

}
