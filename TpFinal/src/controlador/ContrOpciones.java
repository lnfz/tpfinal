package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import vista.Opciones;

public class ContrOpciones implements ActionListener{
	
	private Opciones opc;

	public ContrOpciones() {
		super();
		this.setOpc(new Opciones(this));
		this.getOpc().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();
		if (btn.getText().equals("Volver")) {
			ContrMenu conmen = new ContrMenu();
			opc.dispose();
			conmen.getMen().setVisible(true);
			conmen.getMen().setFocusable(true);
		}
	}

	public Opciones getOpc() {
		return opc;
	}



	public void setOpc(Opciones opc) {
		this.opc = opc;
	}

}
