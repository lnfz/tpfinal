package controlador;

import java.awt.EventQueue;
import java.util.Random;

import modelo.GuardarNumero;
import modelo.ManejarMusica;
import vista.Menu;
import vista.VistaTabla;

public class Main {

	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		Random rnd = new Random();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ContrMenu cm = new ContrMenu();
					cm.getMen().setVisible(true);
					Integer num = rnd.nextInt(3)+1;
					GuardarNumero numero = new GuardarNumero(num);
					ManejarMusica musica = new ManejarMusica(numero.getNum());
					musica.run(1);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	
	}
}
