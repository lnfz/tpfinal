package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import vista.Menu;

public class ContrMenu implements ActionListener{

	private Menu men;
	
	public ContrMenu() {
		super();
		this.setMen(new Menu(this));
		this.getMen().setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		JButton btn = (JButton) e.getSource();
		if (btn.getText().equals("Ranking")){
			ControladorUsuarioDaoImpl cudi = new ControladorUsuarioDaoImpl ();
			cudi.getVt().setFocusable(true);
			cudi.getVt().setVisible(true);
			men.dispose();
			//men.getMusica().stop();
		}else if (btn.getText().equals("Nueva partida")) {
			ControladorSelectRanura csr = new ControladorSelectRanura();
			csr.getSr().setModal(true); //esto del modal averiguar bien
			csr.getSr().setVisible(true);
			men.dispose();
			//men.getMusica().stop();
		}else if (btn.getText().equals("Salir")) {
			men.dispose();
			//men.getMusica().stop();
		}else if (btn.getText().equals("Opciones")) {
			ContrOpciones conop = new ContrOpciones();
			conop.getOpc().setModal(true);
			conop.getOpc().setVisible(true);
			conop.getOpc().setFocusable(true);
			men.dispose();
			//men.getMusica().stop();
		}
	}

	public Menu getMen() {
		return men;
	}

	public void setMen(Menu men) {
		this.men = men;
	}

}
