package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorUsuarioDaoImpl;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VistaTabla extends JFrame {

	private JPanel contentPane;
	private JTable usuarios;
	private ControladorUsuarioDaoImpl cui;
	private JButton btnVolver;

	public VistaTabla(ControladorUsuarioDaoImpl cui) {
		setUndecorated(true);
		this.setCui(cui);
		addWindowListener(getCui());//para que cuando inicie salga el escuchador y actualize la vista
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 430, 232);
		contentPane.add(scrollPane);
		
		usuarios = new JTable();
		usuarios.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Id_Usuario", "Nombre", "Apellido", "Score"
			}
		));
		scrollPane.setViewportView(usuarios);
		
		btnVolver = new JButton("Volver");
		btnVolver.addActionListener(getCui());
		btnVolver.setBounds(23, 254, 102, 35);
		contentPane.add(btnVolver);
		
		this.setLocationRelativeTo(null);
	}

	public JTable getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(JTable usuarios) {
		this.usuarios = usuarios;
	}

	public ControladorUsuarioDaoImpl getCui() {
		return cui;
	}

	public void setCui(ControladorUsuarioDaoImpl cui) {
		this.cui = cui;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public void setBtnVolver(JButton btnVolver) {
		this.btnVolver = btnVolver;
	}
}
