package vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorSelectRanura;

import javax.swing.JTextField;
import javax.swing.JEditorPane;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import javax.swing.JToggleButton;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;
import javax.swing.JSpinner;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.JSplitPane;
import javax.swing.JLayeredPane;
import javax.swing.JInternalFrame;
import javax.swing.JDesktopPane;
import javax.swing.JScrollPane;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class SelectRanura extends JDialog {

	private ControladorSelectRanura csr;
	private final JPanel contentPanel = new JPanel();
	private JTextField textUser1;
	private JTextField textScore1;
	private JTextField textUser2;
	private JTextField textScore2;
	private JTextField textUser3;
	private JTextField textScore3;
	private JRadioButton btn1;
	private JRadioButton btn2;
	private JRadioButton btn3;

	/*public static void main(String[] args) {
		try {
			SelectRanura dialog = new SelectRanura();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public SelectRanura(ControladorSelectRanura csr) {
		setUndecorated(true);
		this.setCsr(csr);
		setBounds(100, 100, 450, 313);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JPanel panel1 = new JPanel();
		panel1.setBounds(69, 55, 345, 64);
		panel1.setBackground(new Color(192, 192, 192));
		contentPanel.add(panel1);
		panel1.setLayout(null);
		
		JLabel lblUser1 = new JLabel("User:");
		lblUser1.setBounds(51, 14, 46, 14);
		panel1.add(lblUser1);
		
		textUser1 = new JTextField();
		textUser1.setEditable(false);
		textUser1.setBounds(130, 12, 118, 17);
		panel1.add(textUser1);
		textUser1.setColumns(10);
		
		JLabel lblScore1 = new JLabel("score: ");
		lblScore1.setBounds(51, 39, 46, 14);
		panel1.add(lblScore1);
		
		textScore1 = new JTextField();
		textScore1.setEditable(false);
		textScore1.setColumns(10);
		textScore1.setBounds(130, 37, 74, 17);
		panel1.add(textScore1);
		
		btn1 = new JRadioButton("1");
		btn1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (getBtn1().isSelected())
				{
					getBtn2().setEnabled(false);
					getBtn3().setEnabled(false);
				} else
				{
					getBtn2().setEnabled(true);
					getBtn3().setEnabled(true);
				}
				
			}
		});
		btn1.setBounds(5, 76, 40, 23);
		contentPanel.add(btn1);
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(null);
		panel2.setBackground(Color.LIGHT_GRAY);
		panel2.setBounds(69, 130, 345, 64);
		contentPanel.add(panel2);
		
		JLabel labelUser2 = new JLabel("User:");
		labelUser2.setBounds(51, 14, 46, 14);
		panel2.add(labelUser2);
		
		textUser2 = new JTextField();
		textUser2.setEditable(false);
		textUser2.setColumns(10);
		textUser2.setBounds(130, 12, 118, 17);
		panel2.add(textUser2);
		
		JLabel labelScore2 = new JLabel("score: ");
		labelScore2.setBounds(51, 39, 46, 14);
		panel2.add(labelScore2);
		
		textScore2 = new JTextField();
		textScore2.setEditable(false);
		textScore2.setColumns(10);
		textScore2.setBounds(130, 37, 74, 17);
		panel2.add(textScore2);
		
		btn2 = new JRadioButton("2");
		btn2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (getBtn2().isSelected())
				{
					getBtn1().setEnabled(false);
					getBtn3().setEnabled(false);
				} else
				{
					getBtn1().setEnabled(true);
					getBtn3().setEnabled(true);
				}
			}
		});
		btn2.setBounds(5, 150, 40, 23);
		contentPanel.add(btn2);
		
		JPanel panel3 = new JPanel();
		panel3.setLayout(null);
		panel3.setBackground(Color.LIGHT_GRAY);
		panel3.setBounds(69, 205, 345, 64);
		contentPanel.add(panel3);
		
		JLabel labelUser3 = new JLabel("User:");
		labelUser3.setBounds(51, 14, 46, 14);
		panel3.add(labelUser3);
		
		textUser3 = new JTextField();
		textUser3.setEditable(false);
		textUser3.setColumns(10);
		textUser3.setBounds(130, 12, 118, 17);
		panel3.add(textUser3);
		
		JLabel labelScore3 = new JLabel("score: ");
		labelScore3.setBounds(51, 39, 46, 14);
		panel3.add(labelScore3);
		
		textScore3 = new JTextField();
		textScore3.setEditable(false);
		textScore3.setColumns(10);
		textScore3.setBounds(130, 37, 74, 17);
		panel3.add(textScore3);
		
		btn3 = new JRadioButton("3");
		btn3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (getBtn3().isSelected())
				{
					getBtn2().setEnabled(false);
					getBtn1().setEnabled(false);
				} else
				{
					getBtn2().setEnabled(true);
					getBtn1().setEnabled(true);
				}
			}
		});
		btn3.setBounds(5, 225, 40, 23);
		contentPanel.add(btn3);
		
		JLabel lblTitulo = new JLabel("Seleccione una Ranura");
		lblTitulo.setFont(new Font("Matura MT Script Capitals", Font.PLAIN, 25));
		lblTitulo.setBounds(94, 11, 258, 33);
		contentPanel.add(lblTitulo);
		
		JLabel lblIndicador1 = new JLabel("*");
		lblIndicador1.setForeground(new Color(255, 0, 0));
		lblIndicador1.setBounds(10, 65, 15, 14);
		contentPanel.add(lblIndicador1);
		
		JLabel lblIndicador2 = new JLabel("*");
		lblIndicador2.setForeground(Color.RED);
		lblIndicador2.setBounds(10, 140, 15, 14);
		contentPanel.add(lblIndicador2);
		
		JLabel lblIndicador3 = new JLabel("*");
		lblIndicador3.setForeground(Color.RED);
		lblIndicador3.setBounds(10, 216, 15, 14);
		contentPanel.add(lblIndicador3);
		//{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("SIGUIENTE");
				//okButton.setActionCommand("SIGUIENTE");//estaba automaticamente en OK
				okButton.addActionListener(getCsr());
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("CANCELAR");
				//cancelButton.setActionCommand("CANCELAR"); //estaba automaticamente en Cancelar
				cancelButton.addActionListener(getCsr());
				buttonPane.add(cancelButton);
			}
		//}
		this.setLocationRelativeTo(null);
	}

	public ControladorSelectRanura getCsr() {
		return csr;
	}

	public void setCsr(ControladorSelectRanura csr) {
		this.csr = csr;
	}

	public JTextField getTextUser1() {
		return textUser1;
	}

	public void setTextUser1(JTextField textUser1) {
		this.textUser1 = textUser1;
	}

	public JTextField getTextScore1() {
		return textScore1;
	}

	public void setTextScore1(JTextField textScore1) {
		this.textScore1 = textScore1;
	}

	public JTextField getTextUser2() {
		return textUser2;
	}

	public void setTextUser2(JTextField textUser2) {
		this.textUser2 = textUser2;
	}

	public JTextField getTextScore2() {
		return textScore2;
	}

	public void setTextScore2(JTextField textScore2) {
		this.textScore2 = textScore2;
	}

	public JTextField getTextUser3() {
		return textUser3;
	}

	public void setTextUser3(JTextField textUser3) {
		this.textUser3 = textUser3;
	}

	public JTextField getTextScore3() {
		return textScore3;
	}

	public void setTextScore3(JTextField textScore3) {
		this.textScore3 = textScore3;
	}

	public JRadioButton getBtn1() {
		return btn1;
	}

	public void setBtn1(JRadioButton btn1) {
		this.btn1 = btn1;
	}

	public JRadioButton getBtn2() {
		return btn2;
	}

	public void setBtn2(JRadioButton btn2) {
		this.btn2 = btn2;
	}

	public JRadioButton getBtn3() {
		return btn3;
	}

	public void setBtn3(JRadioButton btn3) {
		this.btn3 = btn3;
	}
}
