package vista;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.ImageIcon;

public class ImagenMenu extends javax.swing.JPanel{

	public ImagenMenu () {
		this.setSize(400, 200);
	}
	
	public void paintComponent (Graphics g) {
		Dimension tam = getSize();
		ImageIcon imagenfondo = new ImageIcon(getClass().getResource("/Imagenes/fondoMenu.jpg"));
		g.drawImage(imagenfondo.getImage(), 0, 0, tam.width, tam.height, null);
		setOpaque(false);
		super.paintComponent(g);
	}
}
