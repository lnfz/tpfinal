package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorJuego;

import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Dialog.ModalExclusionType;
import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JProgressBar;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.SwingConstants;

public class Juego extends JFrame {

	private ControladorJuego cj;
	private JPanel contentPane;
	private JButton btnAtaque;
	private JButton btnBloqueo;
	private JButton btnEspecial;
	private JProgressBar BarraVidaJugador;
	private JProgressBar BarraVidaEnemigo;
	private JLabel lblJugador;
	private JLabel lblEnemigo;
	private JProgressBar BarraDefensaEnemigo;
	private JProgressBar BarraDefensaJugador;
	private JLabel lblNumeros;
	private JLabel lblComienzo;
	private JLabel lblGameOver;
	private JLabel lblRonda;

	public Juego(ControladorJuego cj) {
		setUndecorated(true);
		this.setCj(cj);
		setResizable(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setSize(new Dimension(800, 700));
		setMaximumSize(new Dimension(1000, 1000));
		setMinimumSize(new Dimension(920, 750));
		setMaximizedBounds(new Rectangle(100, 20, 920, 750));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 580, 402);
		/*contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);*/
		
		btnAtaque = new JButton("ataque");
		btnAtaque.addActionListener(this.getCj());
		btnAtaque.setBounds(10, 11, 122, 50);
		//contentPane.add(btnAtaque);
		
		btnEspecial = new JButton("especial");
		btnEspecial.addActionListener(this.getCj());
		btnEspecial.setBounds(337, 11, 122, 50);
		//contentPane.add(btnEspecial);
		
		btnBloqueo = new JButton("bloqueo");
		btnBloqueo.addActionListener(this.getCj());
		btnBloqueo.setBounds(175, 11, 122, 50);
		//contentPane.add(btnBloqueo);
		
		BarraVidaJugador = new JProgressBar();
		BarraVidaJugador.setFont(new Font("Tahoma", Font.BOLD, 11));
		BarraVidaJugador.setForeground(Color.RED);
		BarraVidaJugador.setBackground(Color.BLACK);
		BarraVidaJugador.setString("");
		BarraVidaJugador.setStringPainted(true);
		BarraVidaJugador.setOpaque(true);
		BarraVidaJugador.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		BarraVidaJugador.setMaximum(900);
		//BarraVidaJugador.setMaximum(900);
		BarraVidaJugador.setBounds(523, 31, 296, 14);
		//contentPane.add(BarraVidaJugador);
		
		JLabel lblVidaJugador = new JLabel("Vida");
		lblVidaJugador.setForeground(new Color(255, 255, 255));
		lblVidaJugador.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblVidaJugador.setBounds(835, 28, 56, 14);
		//contentPane.add(lblVidaJugador);
		
		BarraVidaEnemigo = new JProgressBar();
		BarraVidaEnemigo.setFont(new Font("Tahoma", Font.BOLD, 11));
		BarraVidaEnemigo.setForeground(Color.RED);
		BarraVidaEnemigo.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		BarraVidaEnemigo.setBackground(new Color(0, 0, 0));
		BarraVidaEnemigo.setStringPainted(true);
		BarraVidaEnemigo.setString("");
		BarraVidaEnemigo.setOpaque(true);
		BarraVidaEnemigo.setMaximum(900);
		//BarraVidaEnemigo.setMaximum(900);
		BarraVidaEnemigo.setBounds(523, 79, 296, 14);
		//contentPane.add(BarraVidaEnemigo);
		
		JLabel lblVidaEnemigo = new JLabel("Enemigo");
		lblVidaEnemigo.setForeground(new Color(255, 255, 255));
		lblVidaEnemigo.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblVidaEnemigo.setBounds(835, 76, 75, 14);
		//contentPane.add(lblVidaEnemigo);
		
		lblJugador = new JLabel("Icono Jugador");
		
		/*
		BufferedImage img =null;
		try {
			String ruta = System.getProperty("user.dir");
			img = ImageIO.read(new File(ruta+"\\src\\Imagenes\\85696044-pixel-arte-barbudo-personaje-de-h�roe-con-el-cabo-rojo-y-la-espada-listo-para-luchar.png"));
			img =(BufferedImage) escalarImage(img, 566, 543);
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		lblJugador.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/JAlqAtack 1.png")));
		lblJugador.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblJugador.setBounds(106, 361, 218, 224);
		//contentPane.add(lblJugador);
		
		
		lblEnemigo = new JLabel("Icono Enemigo");
		lblEnemigo.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/BerkAtack 1.png")));
		lblEnemigo.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblEnemigo.setBounds(641, 388, 145, 171);
		//contentPane.add(lblArena);

		ImagenJuego im = new ImagenJuego();
		im.setBorder(new EmptyBorder(5, 5, 5, 5));
		im.setLayout(null);
		setContentPane(im);
		im.setLayout(null);
		this.setLocationRelativeTo(null);
		
		im.add(BarraVidaEnemigo);
		im.add(BarraVidaJugador);
		im.add(btnAtaque);
		im.add(btnBloqueo);
		im.add(btnEspecial);
		im.add(lblJugador);
		im.add(lblEnemigo);
		im.add(lblVidaEnemigo);
		im.add(lblVidaJugador);
		
		BarraDefensaJugador = new JProgressBar();
		BarraDefensaJugador.setFont(new Font("Arial Black", Font.BOLD, 10));
		BarraDefensaJugador.setString("");
		BarraDefensaJugador.setStringPainted(true);
		BarraDefensaJugador.setForeground(new Color(128, 128, 128));
		BarraDefensaJugador.setMaximum(800);
		BarraDefensaJugador.setOpaque(true);
		BarraDefensaJugador.setBounds(523, 11, 296, 9);
		im.add(BarraDefensaJugador);
		
		BarraDefensaEnemigo = new JProgressBar();
		BarraDefensaEnemigo.setString("");
		BarraDefensaEnemigo.setStringPainted(true);
		BarraDefensaEnemigo.setFont(new Font("Arial Black", Font.BOLD, 10));
		BarraDefensaEnemigo.setMaximum(800);
		BarraDefensaEnemigo.setForeground(new Color(128, 128, 128));
		BarraDefensaEnemigo.setBounds(523, 59, 296, 9);
		im.add(BarraDefensaEnemigo);
		
		lblNumeros = new JLabel("");
		lblNumeros.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNumeros.setBounds(390, 219, 127, 106);
		im.add(lblNumeros);
		
		lblComienzo = new JLabel("");
		lblComienzo.setAutoscrolls(true);
		lblComienzo.setBounds(338, 267, 229, 58);
		im.add(lblComienzo);
		
		lblGameOver = new JLabel("");
		lblGameOver.setBounds(269, 121, 436, 378);
		im.add(lblGameOver);
		
		lblRonda = new JLabel("");
		lblRonda.setForeground(new Color(32, 178, 170));
		lblRonda.setFont(new Font("Forte", Font.BOLD, 60));
		lblRonda.setBounds(348, 267, 248, 94);
		im.add(lblRonda);
		
	}
	
	private Image escalarImage(Image srcImg, int w, int h) {
		BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = img.createGraphics();
		
		g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2.drawImage(srcImg, 0, 0, w, h, null);
		g2.dispose();
		return img;
	}

	public JButton getBtnAtaque() {
		return btnAtaque;
	}

	public void setBtnAtaque(JButton btnAtaque) {
		this.btnAtaque = btnAtaque;
	}

	public JButton getBtnBloqueo() {
		return btnBloqueo;
	}

	public void setBtnBloqueo(JButton btnBloqueo) {
		this.btnBloqueo = btnBloqueo;
	}

	public JButton getBtnEspecial() {
		return btnEspecial;
	}

	public void setBtnEspecial(JButton btnEspecial) {
		this.btnEspecial = btnEspecial;
	}

	public JProgressBar getBarraVidaJugador() {
		return BarraVidaJugador;
	}

	public void setBarraVidaJugador(JProgressBar barraVidaJugador) {
		BarraVidaJugador = barraVidaJugador;
	}

	public JProgressBar getBarraVidaEnemigo() {
		return BarraVidaEnemigo;
	}

	public void setBarraVidaEnemigo(JProgressBar barraVidaEnemigo) {
		BarraVidaEnemigo = barraVidaEnemigo;
	}

	public ControladorJuego getCj() {
		return cj;
	}

	public void setCj(ControladorJuego cj) {
		this.cj = cj;
	}

	public JProgressBar getBarraDefensaEnemigo() {
		return BarraDefensaEnemigo;
	}

	public void setBarraDefensaEnemigo(JProgressBar barraDefensaEnemigo) {
		BarraDefensaEnemigo = barraDefensaEnemigo;
	}

	public JProgressBar getBarraDefensaJugador() {
		return BarraDefensaJugador;
	}

	public void setBarraDefensaJugador(JProgressBar barraDefensaJugador) {
		BarraDefensaJugador = barraDefensaJugador;
	}

	public JLabel getLblNumeros() {
		return lblNumeros;
	}

	public void setLblNumeros(JLabel lblComienzo) {
		this.lblNumeros = lblComienzo;
	}

	public JLabel getLblComienzo() {
		return lblComienzo;
	}

	public void setLblComienzo(JLabel lblComienzo) {
		this.lblComienzo = lblComienzo;
	}

	public JLabel getLblGameOver() {
		return lblGameOver;
	}

	public void setLblGameOver(JLabel lblGameOver) {
		this.lblGameOver = lblGameOver;
	}

	public JLabel getLblRonda() {
		return lblRonda;
	}

	public void setLblRonda(JLabel lblRonda) {
		this.lblRonda = lblRonda;
	}

	public JLabel getLblJugador() {
		return lblJugador;
	}

	public void setLblJugador(JLabel lblJugador) {
		this.lblJugador = lblJugador;
	}

	public JLabel getLblEnemigo() {
		return lblEnemigo;
	}

	public void setLblEnemigo(JLabel lblEnemigo) {
		this.lblEnemigo = lblEnemigo;
	}
}
