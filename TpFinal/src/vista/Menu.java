package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.sun.media.jfxmedia.AudioClip;

import controlador.ContrMenu;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Frame;
import java.awt.Window.Type;

public class Menu extends JFrame {

	private JPanel contentPane;
	private ContrMenu conmen;
	private JButton btnOpciones;
	private JButton btnRanking;
	private JButton btnNuevaPartida;
	//private Clip musica;
	
	public JButton getBtnOpciones() {
		return btnOpciones;
	}

	public void setBtnOpciones(JButton btnOpciones) {
		this.btnOpciones = btnOpciones;
	}

	public JButton getBtnRanking() {
		return btnRanking;
	}

	public void setBtnRanking(JButton btnRanking) {
		this.btnRanking = btnRanking;
	}

	public JButton getBtnNuevaPartida() {
		return btnNuevaPartida;
	}

	public void setBtnNuevaPartida(JButton btnNuevaPartida) {
		this.btnNuevaPartida = btnNuevaPartida;
	}

	public JButton getBtnContinuar() {
		return btnContinuar;
	}

	public void setBtnContinuar(JButton btnContinuar) {
		this.btnContinuar = btnContinuar;
	}

	private JButton btnContinuar;
	private JButton btnSalir;
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu frame = new Menu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	public Menu(ContrMenu conmen) {
		super("Soul of Destiny");
		setResizable(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		setMaximizedBounds(new Rectangle(100, 20, 920, 750));
		setMinimumSize(new Dimension(920, 750));
		this.setConmen(conmen);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		/*contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);*/
		
		btnNuevaPartida = new JButton("Nueva partida");
		btnNuevaPartida.addActionListener(getConmen());
		btnNuevaPartida.setBounds(68, 303, 135, 29);
		//contentPane.add(btnNuevaPartida);
		
		btnContinuar = new JButton("Continuar");
		btnContinuar.setEnabled(false);
		btnContinuar.setBounds(68, 244, 135, 29);
		//contentPane.add(btnContinuar);
		
		btnRanking = new JButton("Ranking");
		btnRanking.addActionListener(getConmen());
		btnRanking.setBounds(68, 363, 135, 29);
		//contentPane.add(btnRanking);
		
		btnOpciones = new JButton("Opciones");
		btnOpciones.setBounds(68, 423, 135, 29);
		btnOpciones.addActionListener(getConmen());
		//contentPane.add(btnOpciones);
		
		JLabel lblSoulOfDestiny = new JLabel("       Soul of Destiny");
		lblSoulOfDestiny.setForeground(new Color(0, 0, 0));
		lblSoulOfDestiny.setFont(new Font("Andalus", Font.PLAIN, 50));
		lblSoulOfDestiny.setBounds(219, 60, 495, 96);
		//contentPane.add(lblSoulOfDestiny);
		//contentPane.add(im);
		
		/*try {
			//Clip sonido = AudioSystem.getClip();
			String ruta = System.getProperty("user.dir");
			File a = new File (ruta + "\\TpFinal\\src\\Musica\\Menu.wav");
			musica = AudioSystem.getClip();
			musica.open(AudioSystem.getAudioInputStream(a));
			musica.start();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/		
		ImagenMenu im = new ImagenMenu();
		im.setBorder(new EmptyBorder(5, 5, 5, 5));
		im.setLayout(null);
		setContentPane(im);
		im.setLayout(null);
		im.add(lblSoulOfDestiny);
		im.add(btnContinuar);
		im.add(btnNuevaPartida);
		im.add(btnRanking);
		im.add(btnOpciones);
		
		btnSalir = new JButton("Salir");
		btnSalir.setBounds(68, 480, 135, 29);
		btnSalir.addActionListener(getConmen());
		im.add(btnSalir);

		this.setLocationRelativeTo(null);
	}

	/*
	public void cambiar() {
		JPanel panel1 = new JPanel();
		panel1.setBounds(28, 11, 257, 93);

		JLabel lblPanel = new JLabel("PANEL2");
		panel1.add(lblPanel);
		
		
		getContentPane().remove(panel);
		getContentPane().add(panel1);
	    panel1.validate();
	}
	*/
	
	public ContrMenu getConmen() {
		return conmen;
	}

	public void setConmen(ContrMenu conmen) {
		this.conmen = conmen;
	}

	/*public Clip getMusica() {
		return musica;
	}

	public void setMusica(Clip musica) {
		this.musica = musica;
	}*/
}
