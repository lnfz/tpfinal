package vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ContrNuevoUs;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

import java.awt.Component;
import javax.swing.Box;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NuevoUsuario extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtNombre;
	private ContrNuevoUs cnu;
	private JButton cancelButton;
	private JButton okButton;
	private JComboBox comboBox;
	private JComboBox comboBox_1;
	private JLabel lblFotoPersonaje;

	/*public static void main(String[] args) {
		try {
			NuevoUsuario dialog = new NuevoUsuario();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public NuevoUsuario(ContrNuevoUs cnu) {
		setUndecorated(true);
		this.setCnu(cnu);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			okButton = new JButton("OK");
			okButton.setBounds(339, 245, 81, 23);
			contentPanel.add(okButton);
			okButton.addActionListener(getCnu());
			//okButton.setActionCommand("OK");
			getRootPane().setDefaultButton(okButton);
		}
		{
			cancelButton = new JButton("Cancel");
			cancelButton.setBounds(23, 245, 81, 23);
			contentPanel.add(cancelButton);
			cancelButton.addActionListener(getCnu());
			//cancelButton.setActionCommand("Cancel");
		}
		
		JLabel lblNuevoUsuario = new JLabel("Nuevo Usuario");
		lblNuevoUsuario.setFont(new Font("Matura MT Script Capitals", Font.PLAIN, 14));
		lblNuevoUsuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblNuevoUsuario.setBounds(134, 11, 159, 23);
		contentPanel.add(lblNuevoUsuario);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(83, 63, 46, 14);
		contentPanel.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(149, 60, 192, 20);
		contentPanel.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblSeleccionePersonaje = new JLabel("Seleccione Personaje");
		lblSeleccionePersonaje.setBounds(10, 104, 148, 14);
		contentPanel.add(lblSeleccionePersonaje);
		
		comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				switch (getComboBox().getSelectedItem().toString()) {
				case "Alquimista":
					getLblFotoPersonaje().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/JAlqStill 1.png")));
					break;
				case "Berserker":
					getLblFotoPersonaje().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/JBerkStill 1.png")));
					break;
				case "Slave":
					getLblFotoPersonaje().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/JSlaStill 1.png")));
					break;
				default:
					break;
				}
				
				
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Alquimista", "Berserker", "Slave"}));
		comboBox.setBounds(149, 101, 87, 20);
		contentPanel.add(comboBox);
		
		JLabel lblSeleccioneDificultad = new JLabel("Seleccione Dificultad");
		lblSeleccioneDificultad.setBounds(10, 181, 148, 14);
		contentPanel.add(lblSeleccioneDificultad);
		
		comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Facil", "Normal", "Dificil"}));
		comboBox_1.setBounds(149, 178, 87, 20);
		contentPanel.add(comboBox_1);
		
		lblFotoPersonaje = new JLabel("");
		lblFotoPersonaje.setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/JAlqStill 1.png")));
		lblFotoPersonaje.setBounds(277, 104, 143, 107);
		contentPanel.add(lblFotoPersonaje);
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(null);
		}
		this.setLocationRelativeTo(null);
	}

	public ContrNuevoUs getCnu() {
		return cnu;
	}

	public void setCnu(ContrNuevoUs cnu) {
		this.cnu = cnu;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}


	public JButton getOkButton() {
		return okButton;
	}

	public void setOkButton(JButton okButton) {
		this.okButton = okButton;
	}

	public JComboBox getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}

	public JComboBox getComboBox_1() {
		return comboBox_1;
	}

	public void setComboBox_1(JComboBox comboBox_1) {
		this.comboBox_1 = comboBox_1;
	}

	public JLabel getLblFotoPersonaje() {
		return lblFotoPersonaje;
	}

	public void setLblFotoPersonaje(JLabel lblFotoPersonaje) {
		this.lblFotoPersonaje = lblFotoPersonaje;
	}
}
