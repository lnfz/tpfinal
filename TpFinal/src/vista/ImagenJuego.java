package vista;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.ImageIcon;

public class ImagenJuego extends javax.swing.JPanel
{
	public ImagenJuego () {
		this.setSize(400, 200);
	}
	
	public void paintComponent (Graphics g) {
		Dimension tam = getSize();
		ImageIcon imagenfondo = new ImageIcon(getClass().getResource("/Imagenes/images (2).jpg"));
		g.drawImage(imagenfondo.getImage(), 0, 0, tam.width, tam.height, null);
		setOpaque(false);
		super.paintComponent(g);
	}
	
}
