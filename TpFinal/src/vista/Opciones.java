package vista;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ContrOpciones;
import modelo.GuardarNumero;
import modelo.ManejarMusica;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

public class Opciones extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private ContrOpciones conop;
	private JButton btnVolver;
	private JComboBox comboBox;

	/*public static void main(String[] args) {
		try {
			Opciones dialog = new Opciones();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public Opciones(ContrOpciones conop) {
		setUndecorated(true);
		this.setConop(conop);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		btnVolver = new JButton("Volver");
		btnVolver.setBounds(10, 227, 89, 23);
		btnVolver.addActionListener(getConop());
		contentPanel.add(btnVolver);
		
		JLabel lblElegirMusica = new JLabel("Elegir musica");
		lblElegirMusica.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblElegirMusica.setHorizontalAlignment(SwingConstants.CENTER);
		lblElegirMusica.setBounds(157, 11, 127, 23);
		contentPanel.add(lblElegirMusica);
		
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Musica 1", "Musica 2 ", "Musica 3"}));
		comboBox.setBounds(157, 60, 127, 20);
		contentPanel.add(comboBox);
		
		this.setLocationRelativeTo(null);
	}

	public ContrOpciones getConop() {
		return conop;
	}

	public void setConop(ContrOpciones conop) {
		this.conop = conop;
	}

	public JComboBox getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}
}
