package modelo;

import javax.swing.ImageIcon;

import controlador.ContrMenu;
import controlador.ControladorJuego;
import vista.Juego;
import vista.Menu;

public class GameOver extends Thread{

private ControladorJuego cj;
private UsuarioDaoImpl udi;
	
	public GameOver (ControladorJuego cj)
	{
		this.setCj(cj);	
		this.setUdi(new UsuarioDaoImpl());
	}
	
	public void run()
	{
		
		try {
			this.getCj().getJuego().getLblGameOver().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/GameOver.jpg")));
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//MANEJAR LA BD AQUI
		if (this.getCj().getRonda() == 4 || this.getCj().getRonda() == 9 || this.getCj().getRonda() == 15) //15 por que al final se suma 1 de mas
		{
			this.getCj().getU().setRonda(String.valueOf(this.getCj().getRonda()));
			this.getCj().getU().setVida(String.valueOf(this.getCj().getU().getP().getVida().intValue()));
			this.getCj().getU().setDefensa(String.valueOf(this.getCj().getU().getP().getDefensa().intValue()));
			this.getCj().getU().setAtaque(String.valueOf(this.getCj().getU().getP().getAtaque().intValue()));
			
			this.getUdi().modificarUsuario(this.getCj().getU());
		}
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//VOLVER AL MENU
		
		this.getCj().getJuego().dispose();
		ContrMenu menu = new ContrMenu();
		menu.getMen().setVisible(true);
		menu.getMen().setFocusable(true);
		
		
	}

	public ControladorJuego getCj() {
		return cj;
	}

	public void setCj(ControladorJuego cj) {
		this.cj = cj;
	}

	public UsuarioDaoImpl getUdi() {
		return udi;
	}

	public void setUdi(UsuarioDaoImpl udi) {
		this.udi = udi;
	}
	
	
}
