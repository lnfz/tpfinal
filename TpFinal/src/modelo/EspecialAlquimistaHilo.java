package modelo;

import javax.swing.ImageIcon;

import controlador.ControladorJuego;

public class EspecialAlquimistaHilo extends Thread{

	private ControladorJuego C;
	private Personaje p;
	private Integer k;
	
	public EspecialAlquimistaHilo(ControladorJuego C, Personaje p, Integer k) {
		this.setC(C);
		this.setP(p);
		this.setK(k);
	}
	
	public void run ()
	{
		if (this.getK() == 1)
		{
			
			switch (this.getP().getNombrePersonaje()) 
			{
			case "Slave":
				this.getC().desactivarBotones();
				for (int i = 1; i <= 39; i++) 
				{
				
					try {
						this.getC().getJuego().getLblJugador().setIcon(new ImageIcon(this.getClass().getResource("/Imagenes/JSlaSpecial " + i +".png")));
						Thread.sleep(55);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				this.getC().activarBotones();
				break;
			case "Berserker":
				this.getC().desactivarBotones();
				for (int i = 1; i <= 49; i++) 
				{
				
					try {
						this.getC().getJuego().getLblJugador().setIcon(new ImageIcon(this.getClass().getResource("/Imagenes/JBerkSpecial " + i +".png")));
						Thread.sleep(40);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				this.getC().activarBotones();
				break;
			case "Alquimista":
				this.getC().desactivarBotones();
				for (int i = 1; i <= 15; i++) {
					
					try {
						this.getC().getJuego().getLblJugador().setIcon(new ImageIcon(this.getClass().getResource("/Imagenes/JAlqSpecial " + i +".png")));
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				this.getC().activarBotones();
				break;
			default:
				break;
			}
		
		}
		else if (this.getK() == 2)
		{
			switch (this.getP().getNombrePersonaje()) 
			{
			case "Slave":
				this.getC().desactivarBotones();
				for (int i = 1; i <= 39; i++) 
				{
				
					try {
						this.getC().getJuego().getLblEnemigo().setIcon(new ImageIcon(this.getClass().getResource("/Imagenes/ESlaSpecial " + i +".png")));
						Thread.sleep(55);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
				this.getC().activarBotones();
				break;
			case "Berserker":
				this.getC().desactivarBotones();
					for (int i = 1; i <= 49; i++) 
					{
					
						try {
							this.getC().getJuego().getLblEnemigo().setIcon(new ImageIcon(this.getClass().getResource("/Imagenes/BerkSpecial " + i +".png")));
							Thread.sleep(40);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		
					this.getC().activarBotones();
				break;
			case "Alquimista":
				this.getC().desactivarBotones();
				for (int i = 1; i <= 15; i++) {
					
					try {
						this.getC().getJuego().getLblEnemigo().setIcon(new ImageIcon(this.getClass().getResource("/Imagenes/EAlqSpecial " + i +".png")));
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
				this.getC().activarBotones();
				break;
			default:
				break;
			}
		}

	}

	public ControladorJuego getC() {
		return C;
	}

	public void setC(ControladorJuego c) {
		C = c;
	}

	public Personaje getP() {
		return p;
	}

	public void setP(Personaje p) {
		this.p = p;
	}

	public Integer getK() {
		return k;
	}

	public void setK(Integer k) {
		this.k = k;
	}


	
}
