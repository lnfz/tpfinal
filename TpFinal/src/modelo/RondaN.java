package modelo;

import java.awt.Color;

import javax.swing.ImageIcon;

import controlador.ControladorJuego;
import vista.Juego;

public class RondaN extends Thread {

	private ControladorJuego cj;
	
	public RondaN(ControladorJuego cj)
	{
		this.setCj(cj);
	}
	
	public void run ()
	{
		switch (this.getCj().getRonda()) {
		case 4: //4 del arreglo
			try {
				this.getCj().getJuego().getLblRonda().setText("B O S S");
				this.getCj().getJuego().getLblRonda().setForeground(new Color(255, 140, 0));
				Thread.sleep(2000);
				this.getCj().getJuego().getLblRonda().setText("");
				this.getCj().getJuego().getLblRonda().setForeground(new Color (32, 178,170));
				
				Comienzo c = new Comienzo(this.getCj());
				c.start();
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		case 9: //9 del arreglo
			try {
				this.getCj().getJuego().getLblRonda().setText("B O S S");
				this.getCj().getJuego().getLblRonda().setForeground(new Color(255, 140, 0));
				Thread.sleep(2000);
				this.getCj().getJuego().getLblRonda().setText("");
				this.getCj().getJuego().getLblRonda().setForeground(new Color (32, 178,170));
				
				Comienzo c = new Comienzo(this.getCj());
				c.start();
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
			
		case 14:
			try {
				this.getCj().getJuego().getLblRonda().setText("B O S S");
				this.getCj().getJuego().getLblRonda().setForeground(new Color(255, 140, 0));
				Thread.sleep(2000);
				this.getCj().getJuego().getLblRonda().setText("");
				this.getCj().getJuego().getLblRonda().setForeground(new Color (32, 178,170));
				
				Comienzo c = new Comienzo(this.getCj());
				c.start();
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		default:
			try {
				
				this.getCj().getJuego().getLblRonda().setText("Ronda " + (this.getCj().getRonda()+1) );
				Thread.sleep(2000);
				this.getCj().getJuego().getLblRonda().setText("");
				
				Comienzo c = new Comienzo(this.getCj());
				c.start();
				
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			break;
		}
		
		this.getCj().getJuego().getBarraVidaEnemigo().setMaximum(this.getCj().getPe().getVida().intValue());
		this.getCj().getJuego().getBarraVidaEnemigo().setString(this.getCj().getPe().getVida().toString());
		this.getCj().getJuego().getBarraVidaEnemigo().setValue(this.getCj().getPe().getVida().intValue());
		
		this.getCj().getJuego().getBarraDefensaEnemigo().setMaximum(this.getCj().getPe().getDefensa().intValue());
		this.getCj().getJuego().getBarraDefensaEnemigo().setString(this.getCj().getPe().getDefensa().toString());
		this.getCj().getJuego().getBarraDefensaEnemigo().setValue(this.cj.getPe().getDefensa().intValue());

	}

	public ControladorJuego getCj() {
		return cj;
	}

	public void setCj(ControladorJuego cj) {
		this.cj = cj;
	}
	
}
