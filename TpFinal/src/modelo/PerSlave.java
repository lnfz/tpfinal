package modelo;
import java.util.Random;


/*
 * Debemos decidir cuanto de vida, defensa y ataque le corresponde a cada uno
 * Vida = 2500   Defensa = 500  Ataque = 110
 */

public class PerSlave extends Personaje {
	
	public PerSlave(Double vida, Double defensa, Double ataque) {
		super(vida, defensa, ataque);
		this.setNombrePersonaje("Esclavo");
	}
	

	@Override
	public Double ataque() 
	{
		//puede tener dos golpes, golpe normal y el critico que sale al azar
		Random ran = new Random ();
		Double num = ran.nextDouble()*4; //0.0-4.0
		
		if (num <= 3.0)
		{
			num = ran.nextDouble()* this.getAtaque(); 
			//el valor de ataque maximo del personaje
			//el double devuelve valor entre 0 y 1 creo, por lo que se multiplica para obtener el valor deseado
		} else
		{
			num = this.getAtaque() + this.getAtaque() * ran.nextDouble()*0.15; //golpe critico
		}
		
		return num;
	}
	

	@Override
	public void curacion() 
	{
		
		System.out.println("El esclavo se a curado");
	}

	@Override
	public Double ataqueEspecial() 
	{
		
		System.out.println("El esclavo realizo un ataque especial");
		return 0.0;
	}

}
