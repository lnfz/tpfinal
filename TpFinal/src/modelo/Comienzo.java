package modelo;

import java.awt.Font;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import controlador.ControladorJuego;
import vista.Juego;

public class Comienzo extends Thread{

	private ControladorJuego cj;
	
	public Comienzo (ControladorJuego cj)
	{
		this.setCj(cj);	
	}
	
	public void run()
	{
		this.getCj().desactivarBotones();
		try {
			for (int i = 3; i >= 1; i--) {
				
				this.getCj().getJuego().getLblNumeros().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/" + i + ".png")));// poner  icono
				Thread.sleep(1000);
				
			}
			this.getCj().getJuego().getLblNumeros().setIcon(null);
			
			this.getCj().getJuego().getLblComienzo().setIcon(new ImageIcon(Juego.class.getResource("/Imagenes/fight.png")));
			Thread.sleep(1000);
			this.getCj().getJuego().getLblComienzo().setIcon(null);
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//para saber quien comienza
		Random rnd = new Random();
		Integer r = rnd.nextInt(2); //0-1
				
		if (r == 0) //comienza el enemigo 
		{
			this.getCj().Batalla();
		}
		this.getCj().activarBotones();
		
	}
	

	public ControladorJuego getCj() {
		return cj;
	}

	public void setCj(ControladorJuego cj) {
		this.cj = cj;
	}
	
}
