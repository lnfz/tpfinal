package modelo;

import javax.swing.ImageIcon;

import controlador.ControladorJuego;

public class AtacarHilo extends Thread {
	private ControladorJuego C;
	private Personaje p;
	private Integer k;

	public AtacarHilo(ControladorJuego C, Personaje p, Integer k) {
		this.setC(C);
		this.setP(p);
		this.setK(k);
	}

	public void run() {

		if (this.getK() == 1) {
			switch (this.getC().getP().getNombrePersonaje()) {
			case "Slave":
				for (int i = 1; i <= 21; i++) {

					try {
						this.getC().getJuego().getLblJugador().setIcon(
								new ImageIcon(this.getClass().getResource("/Imagenes/JSlaAtack " + i + ".png")));
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			case "Berserker":
				for (int i = 1; i <= 9; i++) {

					try {
						this.getC().getJuego().getLblJugador().setIcon(
								new ImageIcon(this.getClass().getResource("/Imagenes/JBerkAtack " + i + ".png")));
						Thread.sleep(400);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			case "Alquimista":
				for (int i = 1; i <= 3; i++) {

					try {
						this.getC().getJuego().getLblJugador().setIcon(
								new ImageIcon(this.getClass().getResource("/Imagenes/JAlqAtack " + i + ".png")));
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			default:
				break;
			}
		} else if (this.getK() == 2) {
			switch (this.getC().getP().getNombrePersonaje()) {
			case "Slave":
				for (int i = 1; i <= 21; i++) {

					try {
						this.getC().getJuego().getLblJugador().setIcon(
								new ImageIcon(this.getClass().getResource("/Imagenes/ESlaAtack " + i + ".png")));
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			case "Berserker":
				for (int i = 1; i <= 9; i++) {

					try {
						this.getC().getJuego().getLblJugador().setIcon(
								new ImageIcon(this.getClass().getResource("/Imagenes/BerkAtack " + i + ".png")));
						Thread.sleep(400);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			case "Alquimista":
				for (int i = 1; i <= 3; i++) {

					try {
						this.getC().getJuego().getLblJugador().setIcon(
								new ImageIcon(this.getClass().getResource("/Imagenes/EAlqAtack " + i + ".png")));
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				break;
			default:
				break;
			}
		}
	}

	public ControladorJuego getC() {
		return C;
	}

	public void setC(ControladorJuego c) {
		C = c;
	}

	public Personaje getP() {
		return p;
	}

	public void setP(Personaje p) {
		this.p = p;
	}

	public Integer getK() {
		return k;
	}

	public void setK(Integer k) {
		this.k = k;
	}

}
