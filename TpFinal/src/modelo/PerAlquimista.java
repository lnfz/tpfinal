package modelo;
import java.util.Random;

/*
 * Debemos decidir cuanto de vida, defensa y ataque le corresponde a cada uno
 * Vida = 1900   Defensa = 320  Ataque = 80
 */

public class PerAlquimista extends Personaje {

	
	public PerAlquimista(Double vida, Double defensa, Double ataque) {
		super(vida, defensa, ataque);
		this.setNombrePersonaje("Alqumista");
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public Double ataque() 
	{
		Random ran = new Random();
		Double num = ran.nextDouble()*8; //0.0-8.0
		
		if (num <= 4.0)
		{
			//golpe normal
			num = ran.nextDouble()*this.getAtaque();
			num = veneno(num,ran); // el valor de num puede o no modificarse
		} else
		{
			//golpe critico
			num = this.getAtaque() + this.getAtaque()*(ran.nextDouble()*0.15);
			num = veneno(num,ran); // el valor de num puede o no modificarse
		}
		
		System.out.println("El Alquimista a atacado");
		return num;
	}
	
	public Double veneno(Double num, Random ran) //da�a y cura, es una pasiva
	{
		// posee da�o normal, veneno o curacion
		Double num2 = ran.nextDouble()*9; //3.0 - 6.0 - 9.0 
		
		if ( num2 > 3.0 && num2 <= 6.0)
		{
			//da�o por veneno
			num = num + num*0.30; //elegi un valor a lazar
			System.out.println("El Alquimista a sacado un poco de da�o por veneno");
		} else if (num2 > 6.0)
		{
			//curacion
			this.setVida(this.getVida() + this.getVida()*0.02); // se suma vida
			System.out.println("El Alquimista se a curado");
		}
		return num;
	}
	

	@Override
	public void curacion() 
	{
		
		System.out.println("El Alquimista se a defendido");
	}

	@Override
	public Double ataqueEspecial() 
	{
		
		System.out.println("El Alquimista realizo un ataque especial");
		return 0.0;
	}

}
