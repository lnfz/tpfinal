package modelo;
import java.util.Random;

/*
 * Debemos decidir cuanto de vida, defensa y ataque le corresponde a cada uno
 * Vida = 3200   Defensa = 800  Ataque = 50
 */

public class PerBerserker extends Personaje {
	
	public PerBerserker(Double vida, Double defensa, Double ataque) {
		super(vida, defensa, ataque);
		this.setNombrePersonaje("Berserker");
	}


	@Override
	public Double ataque() 
	{
		Random ran = new Random();
		Double num = ran.nextDouble()*8; //0.0-8.0
		
		if (num <= 7.0) //golpe normal
		{
			num = ran.nextDouble()*this.getAtaque();
			System.out.println("El Berserker a atacado");
		} else
		{
			num = this.getAtaque() + this.getAtaque()*(ran.nextDouble()*0.50); //golpe poderoso del berserker
			System.out.println("El Berserker realizo un golpe critico");
		}
		
		
		return num;
	}


	@Override
	public void curacion() 
	{
		
		System.out.println("El Berserker se a curado");
	}

	@Override
	public Double ataqueEspecial() 
	{
		
		System.out.println("El Berserker realizo un ataque especial");
		return 0.0;
	}
}
