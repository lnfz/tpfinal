package modelo;

import java.util.Random;

public abstract class Personaje 
{

	/*
	 *  la defensa se resta junto con la vida? una vez se acabe la defensa comienza a restar la vida?
	 *  la defensa actua sobre la vida? o sea que si un golpe saca 100puntos la defensa haria que baje a 90puntos?
	 *  la defensa se multiplica por X % y se resta con el da�o que recibe?
	 *  
	 *  La defensa sirve para restar un porcentaje de da�o, y esta es fija
	 */
	
	private Double vida;
	private Double defensa;
	private Double ataque;
	private String nombrePersonaje;
	
	
	public Personaje (Double vida, Double defensa, Double ataque)
	{
		this.vida = vida;
		this.defensa = defensa;
		this.ataque = ataque;
	}
	
	
	public abstract Double ataque();
	public abstract void curacion();
	public abstract Double ataqueEspecial();
	
	
	public void defensa(Double ataque)
	{
		System.out.println("personaje se a defendido");
		//solo defensa
		this.setDefensa(this.getDefensa() - (ataque * 0.07));
		
	}
	
	public Boolean recibirDa�o(Double num)
	{
		Double maxDef = 1500.0;
		Double porReducc = this.getDefensa() / maxDef;
		
		if (this.getVida() - (num - (num*porReducc)) <= 0)
		{
			this.setVida(this.getVida()-(num - (num*porReducc))); //solo para probar, luego borrar
			System.out.println("\n" + "Recibio: " + (num - (num*porReducc)) + " de da�o");
			return false; //murio
		}
		this.setVida(this.getVida()-(num - (num*porReducc)));
		System.out.println("\n" + "Recibio: " + (num - (num*porReducc)) + " de da�o");
		return true; //sigue vivo
	}
	
	
	public Double getVida() {
		return vida;
	}
	public void setVida(Double vida) {
		this.vida = vida;
	}
	public Double getDefensa() {
		return defensa;
	}
	public void setDefensa(Double defensa) {
		this.defensa = defensa;
	}
	public Double getAtaque() {
		return ataque;
	}
	public void setAtaque(Double ataque) {
		this.ataque = ataque;
	}


	public String getNombrePersonaje() {
		return nombrePersonaje;
	}


	public void setNombrePersonaje(String nombrePersonaje) {
		this.nombrePersonaje = nombrePersonaje;
	}
	
	
	
}
