package modelo;

public class Usuario {

	private String id_usuario; //integer
	private String nombre; //charvarying
	private String score;
	private String personaje;
	private String vida;
	private String defensa;
	private String ataque;
	private Personaje p;
	private String ronda;
	private String dificultad;
	
	
	public Usuario (String id_usuario, String nombre, String score,String personaje)
	{
		this.setId_usuario(id_usuario);
		this.setNombre(nombre);
		this.setScore(score);
		this.setPersonaje(personaje);
	}
	
	
	public String getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(String id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getScore() {
		return score;
	}


	public void setScore(String score) {
		this.score = score;
	}


	public String getPersonaje() {
		return personaje;
	}


	public void setPersonaje(String personaje) {
		this.personaje = personaje;
	}


	public String getVida() {
		return vida;
	}


	public void setVida(String vida) {
		this.vida = vida;
	}


	public String getDefensa() {
		return defensa;
	}


	public void setDefensa(String defensa) {
		this.defensa = defensa;
	}


	public String getAtaque() {
		return ataque;
	}


	public void setAtaque(String ataque) {
		this.ataque = ataque;
	}


	public Personaje getP() {
		return p;
	}


	public void setP(Personaje p) {
		this.p = p;
	}


	public String getRonda() {
		return ronda;
	}


	public void setRonda(String ronda) {
		this.ronda = ronda;
	}


	public String getDificultad() {
		return dificultad;
	}


	public void setDificultad(String dificultad) {
		this.dificultad = dificultad;
	}
	
}
