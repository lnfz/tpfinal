package modelo;

import java.util.ArrayList;
import java.util.Random;

public class OrgBatalla extends Thread {

	private ArrayList<Personaje> p = new ArrayList<Personaje>();

	public OrgBatalla() {//ArrayList<Personaje> p) {
		//super();
		//this.setP(p);
	}

	public void run() {//Personaje per) {
		
		this.setP(jefes(asignarPersonajes()));
	}

	private ArrayList<Personaje> asignarPersonajes() {
		Random ran = new Random();
		ArrayList<Personaje> pe = new ArrayList<Personaje>();
		for (int i = 0; i <= 14; i++) {
			switch (ran.nextInt(3)) {
			case 0:
				pe.add(new PerSlave(2500.0, 450.0, 150.0));
				break;
			case 1:
				pe.add(new PerBerserker(3000.0, 800.0, 90.0));
				break;
			case 2:
				pe.add(new PerAlquimista(2010.0, 400.0, 120.0));
				break;
			}
			
		}
		return pe;
	}

	private ArrayList<Personaje> jefes(ArrayList<Personaje> p) {
		Personaje pers = p.get(4);
		pers.setAtaque((pers.getAtaque() + (pers.getAtaque() * 0.25)));
		pers.setVida((pers.getVida() + (pers.getVida() * 0.25)));
		pers.setDefensa((pers.getDefensa() + (pers.getDefensa() * 0.25)));
		p.set(9, pers);
		pers.setAtaque((pers.getAtaque() + (pers.getAtaque() * 0.50)));
		pers.setVida((pers.getVida() + (pers.getVida() * 0.50)));
		pers.setDefensa((pers.getDefensa() + (pers.getDefensa() * 0.50)));
		p.set(14, pers);
		/*
		 * pers.setAtaque((pers.getAtaque() + pers.getAtaque()));
		 * pers.setVida((pers.getVida() + pers.getVida()));
		 * pers.setDefensa((pers.getDefensa() + pers.getDefensa())); p.set(15, pers);
		 */
		return p;
	}

	public ArrayList<Personaje> getP() {
		return p;
	}

	public void setP(ArrayList<Personaje> p) {
		this.p = p;
	}

}
