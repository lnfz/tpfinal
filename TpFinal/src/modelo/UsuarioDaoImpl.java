package modelo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDaoImpl implements UsuarioDao{

	@Override
	public Boolean agregarUsuario(Usuario e) {
		ArrayList<String> parametros = new ArrayList<String>();
		
		parametros.add(e.getId_usuario());
		parametros.add(e.getNombre());
		parametros.add(e.getScore());
		parametros.add(e.getPersonaje());
		parametros.add(e.getVida());
		parametros.add(e.getDefensa());
		parametros.add(e.getAtaque());
		parametros.add(e.getRonda());
		parametros.add(e.getDificultad());
		
		String consulta = "insert into usuario(id_usuario,nombre,score,personaje,vida,defensa,ataque,ronda,dificultad) " + " values(?,?,?,?,?,?,?,?,?)";
		
		try {
			return BaseDeDatos.getInstance().manipularEntidades(consulta, parametros);
		} catch (SQLException e1) {
			//si pasa algo malo, es por esta sentemcia LASKDJGKLASJEL
			e1.printStackTrace();
			return null;
		}
		//return null; //si pasa algo malo, es por esta sentemcia LASKDJGKLASJEL
	}


	@Override
	public List<Usuario> buscarUsuario() {
		
		String consulta = "select * from usuario";
		List<Usuario> u = new ArrayList<Usuario>();
		ResultSet rs;
		try {
			rs = BaseDeDatos.getInstance().listarEntidades(consulta);
			try {
				while(rs.next())
				{
					Usuario us;
					
					us = new Usuario (rs.getString("id_usuario"),rs.getString("nombre"),rs.getString("score"),rs.getString("personaje"));
					
					u.add(us);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return u;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return u;
		}
		/*try {
			while(rs.next())
			{
				Usuario us;
				
				us = new Usuario (rs.getString("id_usuario"),rs.getString("nombre"),rs.getString("apellido"),rs.getString("score")); //si hay enteros se castea?
				
				u.add(us);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return u;*/
	}
	
	@Override
	public Integer incrementarId() {
		
		String consulta = "Select count (*) from usuario";//Max (usuario.id_usuario) From usuario";
		//le cambie por que solo tomaba hasta 10 jugadores
		String i = "";
		ResultSet rs = null;
		
		try {
			rs = BaseDeDatos.getInstance().listarEntidades(consulta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if (rs.next())
			{
				i = rs.getString("count");
			}	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Integer.valueOf(i)+1;
	}
	

	@Override
	public Boolean agregarRanura(Usuario e, Integer i) {
		
		String consulta = "update public.ranura set id_usuario =? where id_ranura =?";
		
		
		try {
			return BaseDeDatos.getInstance().modificarEntidad(consulta,e,i);
		} catch (SQLException e1) {
			//si pasa algo malo, es por esta sentemcia LASKDJGKLASJEL
			e1.printStackTrace();
			return null;
		}
	}
	
	
	@Override
	public Boolean modificarUsuario(Usuario e) {
		
		String consulta = "update usuario set score =?, vida =?, defensa =?, ataque =?, ronda =? where id_usuario =?";
		
		try 
		{
			return BaseDeDatos.getInstance().modificarEntidadTablas(consulta, e);
		}catch (SQLException e1)
		{
			e1.printStackTrace();
			return null;
		}

	}
	
	@Override
	public Usuario buscarUsuario(String legajo) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Boolean borrarUsuario(String legajo) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
