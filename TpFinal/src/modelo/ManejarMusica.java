package modelo;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class ManejarMusica extends Thread{
	
	private Clip mus;
	private Integer num;
	
	public ManejarMusica(Integer num) {
		super();
		this.setNum(num);
	}

	public void run(Integer n) {
		mus = ponerMusica(num);
		switch (n) {
		case 1:
			empezarMusica();
			break;
		case 2:
			pararMusica();
			break;
		}
	}
	
	private Clip ponerMusica (Integer num) {
		Clip mu;
		String ruta = System.getProperty("user.dir");
		File a = new File (ruta + "\\TpFinal\\src\\Musica\\Musica " + num + ".wav");
		try {
			mu = AudioSystem.getClip();
			mu.open(AudioSystem.getAudioInputStream(a));
			return mu;
		} catch (LineUnavailableException | IOException | UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private void pararMusica() {
		mus.stop();
	}
	
	private void empezarMusica() {
		mus.start();
	}

	public Clip getMus() {
		return mus;
	}

	public void setMus(Clip mus) {
		this.mus = mus;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

}
