package modelo;

public class GuardarNumero {

	private Integer num;

	public GuardarNumero(Integer num) {
		super();
		this.setNum(num);
	}

	public synchronized Integer getNum() {
		return num;
	}

	public synchronized void setNum(Integer num) {
		this.num = num;
	}
	
}
