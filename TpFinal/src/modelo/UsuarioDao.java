package modelo;
import java.util.List;

public interface UsuarioDao {
	
	Boolean agregarUsuario(Usuario e);
	Usuario buscarUsuario(String legajo);
	List<Usuario> buscarUsuario();
	Boolean borrarUsuario(String legajo);
	Boolean modificarUsuario(Usuario e);
	Integer incrementarId ();
	Boolean agregarRanura(Usuario e,Integer i);
}
