package modelo;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

//todo en el modelo

public class BaseDeDatos {
	private static BaseDeDatos instance;
	private java.sql.Connection connection;
	private String url = "jdbc:postgresql://localhost:5432/Prueba"; // "/sist_cursos" el nombre de la BD
	private String username = "postgres"; // usuario en pgAdmin            //estaba en "postgres"
	private String password = "alumno"; //contraseña que pusiste en pgAdmin// estaba en "alumno" // primero estaba "1234"

	private BaseDeDatos() throws SQLException {
		try {
			Class.forName("org.postgresql.Driver");
			this.connection = DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public static BaseDeDatos getInstance() throws SQLException {
		if (instance == null) {
			instance = new BaseDeDatos();
		} else if (instance.getConnection().isClosed()) {
			instance = new BaseDeDatos();
		}
		return instance;
	}

	
	public ResultSet listarEntidades(String consulta) { //listarEntidades
		ResultSet rs = null;
		try {
			// Se crea un Statement, para realizar la consulta
			Statement s = connection.createStatement();

			// Se realiza la consulta. Los resultados se guardan en el
			// ResultSet rs
			rs = s.executeQuery(consulta);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

	
	
	public ResultSet listarEntidadesParametrizada(String consulta,ArrayList<String>parametros)
    {
        ResultSet rs = null;
        try
        {
            // Se crea un Statement preparado, para realizar la consulta con parámetros
            PreparedStatement s = connection.prepareStatement(consulta);
            int i=0; 
            while(i<parametros.size())
            {
          	  s.setString(i+1, parametros.get(i));
          	  
          	  i++;
            }
            // Se realiza la consulta. Los resultados se guardan en el resultSet rs
            rs = s.executeQuery();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return rs;
    }
    
    public Boolean manipularEntidades(String consulta,ArrayList<String>parametros)
    {
    	boolean retorno=false;
        try
        {
            // Se crea un Statement preparado, para realizar la consulta con parámetros
            PreparedStatement s = connection.prepareStatement(consulta);
            int i=0; 
            while(i<parametros.size())
            {
          	  s.setString(i+1, parametros.get(i));
          	  
          	  i++;
            }
            // Se realiza la consulta. Los resultados se guardan en el resultSet rs
            s.executeUpdate();
            retorno=true;
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return retorno;
    }
    
    public Boolean modificarEntidad(String consulta,Usuario e, Integer i)
    {
    	boolean retorno=false;
        try
        {
        	PreparedStatement s = connection.prepareStatement(consulta);
        	
        	s.setString(1, e.getId_usuario());
        	s.setString(2, String.valueOf(i));
            // Se realiza la consulta. Los resultados se guardan en el resultSet rs
        	s.executeUpdate();
            retorno=true;
        } catch (Exception e1)
        {
            e1.printStackTrace();
        }
        return retorno;
    }
    
    public Boolean modificarEntidadTablas (String consulta, Usuario e) 
    {
    	boolean retorno=false;
        try
        {
        	PreparedStatement s = connection.prepareStatement(consulta);
        	//consulta = update usuario set score =?, vida =?, defensa =?, ataque =?, ronda =? where id_usuario =?
        	s.setString(1, e.getScore());
        	s.setString(2, e.getVida());
        	s.setString(2, e.getDefensa());
        	s.setString(2, e.getAtaque());
        	s.setString(2, e.getRonda());
        	s.setString(2, e.getId_usuario());
            // Se realiza la consulta. Los resultados se guardan en el resultSet rs
        	s.executeUpdate();
            retorno=true;
        } catch (Exception e1)
        {
            e1.printStackTrace();
        }
    	return retorno;
    }
	
	//no tocar es para la conexion de la BD
	public ResultSet consultar(String consulta) throws SQLException {
		System.out.println(consulta);
		return this.getConnection().prepareStatement(consulta).executeQuery();
	}

	
	public int insertar(String consulta) {
		try {
			return this.getConnection().createStatement().executeUpdate(consulta);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return -1;
	}

	
	public java.sql.Connection getConnection() {
		return connection;
	}

}
